/**
 *
 */

#include "tuw_local_planner/tuw_local_planner.h"

#include <opencv/cv.h>
#include <boost/concept_check.hpp>

using namespace cv;
using namespace tuw;

std::map<LocalPlanner::ControlMode, std::string> LocalPlanner::ControlModeName_ = {
    {DEMO, "DEMO"},
    {STOP, "STOP"},
    {WANDERER, "WANDERER"},
    {WANDERER2, "WANDERER2"},
    {WALL_FOLLOWING, "WALL_FOLLOWING"},
    {WALL_FOLLOWING2, "WALL_FOLLOWING2"},
    {GOTO, "GOTO"},
    {GOTO2, "GOTO2"}
};

LocalPlanner::LocalPlanner ( const std::string &ns )
    : loop_count_ ( 0 )
    , figure_local_ ( ns + ", Local View" ) {
	start = std::chrono::system_clock::now();
	end=start;
	startTurn=false;
	state=FORWARD;
	loop_=0;
	counter_=FULLCHECK_PERIOD;
}
/*
 * left turn: positive angle
 * right turn:negative angle
 */





void LocalPlanner::init() {


    figure_local_.init ( config_.map_pix_x, config_.map_pix_y,
                         config_.map_min_x, config_.map_max_x,
                         config_.map_min_y, config_.map_max_y,
                         config_.map_rotation + M_PI/2.0,
                         config_.map_grid_x, config_.map_grid_y );

    cv::putText ( figure_local_.background(), ControlModeName_[ ( ControlMode ) config_.mode],
                  cv::Point ( 5,figure_local_.view().rows-10 ),
                  cv::FONT_HERSHEY_PLAIN, 1, Figure::black, 1, CV_AA );

    /**
     * @ToDo Wanderer
     * changes the Maxima Musterfrau to your name
     **/
    cv::putText ( figure_local_.background(), "Marko Stanisic",
                  cv::Point ( figure_local_.view().cols-250, figure_local_.view().rows-10 ),
                  cv::FONT_HERSHEY_PLAIN, 1, Figure::black, 1, CV_AA );
}

void LocalPlanner::plot() {
    if ( config_.plot_data ) plotLocal();
    cv::waitKey ( 10 );
}

void LocalPlanner::plotLocal() {
    figure_local_.clear();
    for ( size_t i = 0; i < measurement_laser_.size(); i++ ) {
        /**
        * @ToDo Wanderer
        * uses Figure::symbol or Figure::circle to plot the laser measurements in a for loop
        **/
    	/***********************************E D I T**************************************/
    	figure_local_.symbol(measurement_laser_ [i].end_point,Figure::red);
    	/***********************************E D I T**************************************/
    }
    cv::imshow ( figure_local_.title(),figure_local_.view() );
}

void LocalPlanner::move(const double &linear_speed){
	cmd_.set ( linear_speed, STOP_A );
}
void LocalPlanner::turn(const double &angular_speed){
	cmd_.set ( STOP_L, angular_speed );
}

void LocalPlanner::stop(void){
	cmd_.set ( STOP_L, STOP_A );
}
/**
 * *************************************************************************
 * MAIN CONTROL
 */
void LocalPlanner::ai() {
    if ( measurement_laser_.empty() ) {
        cmd_.set ( 0, 0 );
        return;
    }
    switch ( config_.mode ) {
    case STOP:
        cmd_.set ( 0, 0 );
        break;
    case DEMO:
        ai_demo();
        break;
    case WANDERER:
        ai_wanderer();
        break;
    case WANDERER2:
        ai_wanderer2();
        break;
    case WALL_FOLLOWING:
    	ai_test();
    	break;
    default:
        cmd_.set ( 0, 0 );
    }
    loop_count_++;
}

/**
* Demo
**/
void LocalPlanner::ai_demo() {
    double v = 0.0, w = 0.0;
    if ( measurement_laser_.empty() ) {
        v = 0.2, w = -0.02;
    } else {
        if ( measurement_laser_[measurement_laser_.size() / 4].length < 1.0 ) {
            w = 0.4;
        } else {
            v = 0.4;
        }
    }
    cmd_.set ( v, w );
}



/**
 * a distance in front of the robot
 * b distance of the most left/right of the search area I consider
 * r=ROBOT_RADIUS is radius necessary to cover the robot
 * I want to check a maximum area of 2*r (y-axes) in front of the robot, but
 * it depends of the distance to obstacles/wall in front of the robot
 * the search angle(check_angle) in front of the robot
 *   to cover an area of 2*r must  variable
 * sin(check_angle)=r/b;cos(check_angle)=a/b;
 * sin²(check_angle)+cos²(check_angle)=1;b=sqrt(r²+a²)
 * check_angle=arcsin(r²/sqrt(r²+a²))
 * check_angle [rad] to the left and to the right are monitored during drive
 * because that's the only area in that direction where the robot has to watch out
 */

bool LocalPlanner::checkDirection(void){
	double a=measurement_laser_[ROBOT_INDEX_FORWARD_DIRECTION].length;//0 rad angle
	double temp=sqrt((ROBOT_RADIUS*ROBOT_RADIUS)+(a*a));
	double check_angle=asin((ROBOT_RADIUS)/temp);
	double minimum_angle=(ANGLE_SEARCH_CONUS_MINIMUM/2)* M_PI/180;
	check_angle=MAX(minimum_angle,check_angle);
#ifdef INFO_
	ROS_INFO("Robot checks angle of %f rad.\n",check_angle);
#endif
	int max=(int)ceil(check_angle/ANGLE_INCREMENT);
	for(int i=(ROBOT_INDEX_FORWARD_DIRECTION-max);i<(ROBOT_INDEX_FORWARD_DIRECTION+max);i++){
		if(measurement_laser_[i].length<SAFE_ZONE_R)
			return false;
	}
	return true;
}

bool LocalPlanner::checkAllAngles(void){
if(!counter_){
	for(int i=ROBOT_INDEX_MOST_RIGHT;i<=ROBOT_INDEX_MOST_LEFT;i++){
		if(measurement_laser_[i].length<CRITICAL_R){
#ifdef INFO_
			ROS_INFO("FULLCHECK of all ranges,detected an obstacle.");
#endif
			counter_=FULLCHECK_PERIOD;
			return false;
		}
	}
}else{
	counter_--;
}
	return true;
}
//checkRightSide und checkLeftSide vertauscht
bool LocalPlanner::checkLeftSide(void){

	for(int i=ROBOT_INDEX_FORWARD_DIRECTION;i<=ROBOT_INDEX_MOST_LEFT;i++)
	{
		if(measurement_laser_[i].length<=CRITICAL_R){
#ifdef INFO_
			ROS_INFO("checkLeftSide distance[%d]=%f",i,measurement_laser_[i].length);
#endif
			return false;
		}
	}
	return true;
}

bool LocalPlanner::checkRightSide(void){
	for(int i=ROBOT_INDEX_MOST_RIGHT;i<=ROBOT_INDEX_FORWARD_DIRECTION;i++)
	{

		if(measurement_laser_[i].length<=CRITICAL_R){
#ifdef INFO_
			ROS_INFO("checkRightSide distance[%d]=%f",i,measurement_laser_[i].length);
#endif
			return false;
		}
	}

	return true;
}

double EuclidianDistance(Point2D &p1, Point2D &p2)
{
    return sqrt((p1.x() - p2.x()) * (p1.x() - p2.x())  + (p1.y() - p2.y()) * (p1.y() - p2.y()));
}


bool LocalPlanner::turn_robot_and_stop(double angle){
	std::chrono::seconds sec(4);
	start = std::chrono::system_clock::now();
	//std::chrono::duration<double> elapsed_seconds(sec);
	if(!startTurn){
		start = std::chrono::system_clock::now();
		end = start+sec;
		startTurn=true;
		if(angle>0)
			turn(TURN_SPEED_A);
		else
			turn(-TURN_SPEED_A);

	}else if(end < start)
	{
		stop();
		startTurn=false;
		state=FORWARD;

	}
return true;
}

void LocalPlanner::drive_back(int seconds){
	std::chrono::seconds sec(seconds);
	start = std::chrono::system_clock::now();
	if(!startTurn){
		start = std::chrono::system_clock::now();
		end = start+sec;
		startTurn=true;
		this->move(-BACKWARD_L);

	}else if(end < start)
	{
		stop();
		startTurn=false;
		state=RIGHT;
	}
}

void LocalPlanner::ai_test(void)
{
	drive_back(5);
}


/**
* @ToDo Wanderer
* writes one or two wanderer behaviours to keep the robot at least 120sec driving without a crash by exploring as much as possible.
* I know it sounds weird but don't get too fancy.
**/
void LocalPlanner::ai_wanderer() {
   switch(state){

   case FORWARD:
	  if(checkDirection()&&checkAllAngles()){
		this->move(FORWARD_L);
#ifdef INFO_
		ROS_INFO("Robot drives forward until he detects an obstacle.");
#endif
	}else
		if(loop_>= 8){
			state=STOPPED;
		}else{
			state=LEFT;
			loop_++;
			counter_=FULLCHECK_PERIOD;
		}
	break;

   case LEFT:
	   if(checkAllAngles()){
		turn_robot_and_stop(ANGLE_STANDARD_TURN*(M_PI/180));
		loop_=0;
#ifdef INFO_
		ROS_INFO("Robot turn left for %d degrees.",ANGLE_STANDARD_TURN);
#endif
	}else
		state=RIGHT;
		loop_++;
	break;

   case RIGHT:
	if(checkAllAngles()){
		turn_robot_and_stop(-ANGLE_STANDARD_TURN*(M_PI/180));
		loop_=0;
#ifdef INFO_
		ROS_INFO("Robot turn right for %d degrees.",-ANGLE_STANDARD_TURN);
#endif
	}else
		state=BACKWARD;
		loop_++;
	break;

   case BACKWARD:
#ifdef INFO_
		ROS_INFO("Robot drives back for 3 second.");
#endif
		drive_back(3);
		state=LEFT;
	break;
   case STOPPED:
	   loop_=0;
	   counter_=FULLCHECK_PERIOD;
	   stop();
#ifdef INFO_
	   ROS_INFO("Robot STOPPED.");
#endif
	   break;
   default:
	stop();

}
}
/**
	* @ToDo Wanderer
	* OPTIONAL: if you like you can write a other one :-)
	**/
	void LocalPlanner::ai_wanderer2() {
	    cmd_.set ( 0, 0 );
	}
