#include "tuw_local_planner_node.h"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <tf/transform_datatypes.h>

using namespace tuw;
int main ( int argc, char **argv ) {

    ros::init ( argc, argv, "planner_local" );  /// initializes the ros node with default name
    // Set the logging level manually to DEBUG
        ROSCONSOLE_AUTOINIT;
        log4cxx::LoggerPtr my_logger =
        log4cxx::Logger::getLogger( ROSCONSOLE_DEFAULT_NAME );
        my_logger->setLevel(
        ros::console::g_level_lookup[ros::console::levels::Debug]
        );
    ros::NodeHandle n;
    LocalPlannerNode planner ( n );
    planner.init();


    ros::Rate rate ( 10 );  /// ros loop frequence synchronized with the wall time (simulated time)

    while ( ros::ok() ) {

        /// calls your loop
        planner.ai();

        /// sets and publishes velocity commands
        planner.publishMotion();

        /// plots measurements
        planner.plot();

        /// calls all callbacks waiting in the queue
        ros::spinOnce();

        /// sleeps for the time remaining to let us hit our publish rate
        rate.sleep();
    }
    return 0;
}
/**********************DELETE************************************************************/
void MycallbackLaser ( const sensor_msgs::LaserScan &_laser ){

	 ROS_INFO("Callback Function received:%d",_laser.ranges[0]);
}

void LocalPlannerNode::callbackOdometry (const nav_msgs::Odometry &odom) {
    //double a = quaternion2Angle2D (odom.pose.pose.orientation);
    //setState(odom.pose.pose.position.x, odom.pose.pose.position.y, a);
    //ROS_INFO ("POS x = %d, y = %d, ANG = %d\n", odom.pose.pose.position.x,  odom.pose.pose.position.y,0);
}
/**********************DELETE************************************************************/


/**
 * Constructor
 **/
LocalPlannerNode::LocalPlannerNode ( ros::NodeHandle & n )
    : LocalPlanner(ros::NodeHandle("~").getNamespace()), 
    n_ ( n ), 
    n_param_ ( "~" ){

    /**
     * @ToDo Wanderer
     * @see http://wiki.ros.org/roscpp_tutorials/Tutorials/UsingClassMethodsAsCallbacks
     * subscribes the fnc callbackLaser to a topic called "scan"
     * since the simulation is not providing a scan topic /base_scan has to be remaped
     **/
	/***********************************E D I T**************************************/
     sub_laser_ = n.subscribe( "scan",1,&LocalPlannerNode::callbackLaser,this);


     ROS_INFO("Topic name is %s and that topic has %d publishers.\n",sub_laser_.getTopic().c_str(),sub_laser_.getNumPublishers());

     /***********************************E D I T**************************************/
     /***********************************D E L E D E T E**************************************/

     /// subscribes to  odometry values
     sub_odometry_ = n.subscribe("odom", 1, &LocalPlannerNode::callbackOdometry, this);
     ROS_INFO("Topic name is %s and that topic has %d publishers.\n",sub_odometry_.getTopic().c_str(),sub_odometry_.getNumPublishers());

     /***********************************D E L E D E T E**************************************/
    /// defines a publisher for velocity commands
    pub_cmd_ = n.advertise<geometry_msgs::Twist> ( "cmd_vel", 1 );

    reconfigureFnc_ = boost::bind ( &LocalPlannerNode::callbackConfigLocalPlanner, this,  _1, _2 );
    reconfigureServer_.setCallback ( reconfigureFnc_ );
}

void LocalPlannerNode::callbackConfigLocalPlanner ( tuw_local_planner::LocalPlannerConfig &config, uint32_t level ) {
    ROS_INFO ( "callbackConfigLocalPlanner!" );
    config_ = config;
    init();
}

/**
 * copies incoming laser messages to the base class
 * @param laser
 **/
void LocalPlannerNode::callbackLaser( const sensor_msgs::LaserScan &_laser ) {
     /**
     * @ToDo Wanderer
     * @see http://docs.ros.org/api/sensor_msgs/html/msg/LaserScan.html
     * creates a callback which fills the measurement_laser_ with the information from the sensor_msgs::LaserScan.
     * to not forget to compute the measurement_laser_[xy].end_point
     **/
	/***********************************E D I T**************************************/
    measurement_laser_.range_max() =_laser.range_max;  /// @ToDo
    measurement_laser_.range_min() = _laser.range_min; /// @ToDo
    measurement_laser_.resize (270); /// @ToDo rostopic echo /base_scan/ranges[269]
    for ( int i = 0; i < measurement_laser_.size(); i++ ) {
      /// @ToDo _
       measurement_laser_ [i].length  = _laser.ranges[i];
       measurement_laser_ [i].angle   = _laser.angle_min + i*_laser.angle_increment;
       measurement_laser_ [i].end_point  = Point2D (
    		   0.225+(cos(measurement_laser_ [i].angle)*measurement_laser_ [i].length),
			   sin(measurement_laser_ [i].angle)*measurement_laser_ [i].length );
    }
	/***********************************E D I T**************************************/
}


/**
 * Publishes motion commands for a robot
 **/
void LocalPlannerNode::publishMotion () {
    geometry_msgs::Twist cmd;
    /// creates motion command
    cmd.linear.x = cmd_.v();
    cmd.linear.y = 0.;
    cmd.angular.z = cmd_.w();
    /// publishes motion command
    pub_cmd_.publish ( cmd );
}
