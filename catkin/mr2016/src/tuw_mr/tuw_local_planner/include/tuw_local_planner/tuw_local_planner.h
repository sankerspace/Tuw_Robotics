#ifndef LOCAL_PLANNER_H
#define LOCAL_PLANNER_H

#include <vector>
#include <string>
#include <opencv2/opencv.hpp>
#include <tuw_geometry/tuw_geometry.h>
#include <tuw_local_planner/LocalPlannerConfig.h>
#include <log4cxx/logger.h>
#include <ctime>
#include <chrono>

//#define INFO_


//#define ANGLE_ALPHA 10 //grad
#define ANGLE_SEARCH_CONUS_MINIMUM 15//grad
#define ANGLE_INCREMENT 0.0175
#define ANGLE_STANDARD_TURN 30

#define ROBOT_RADIUS 0.40 //meter [m]
#define ROBOT_INDEX_FORWARD_DIRECTION 134
#define ROBOT_INDEX_MOST_LEFT 169
#define ROBOT_INDEX_MOST_RIGHT 0

//ranges
//#define CRITICAL_R	0.23 //[m]
#define CRITICAL_R	0.4 //[m]
#define SAFE_ZONE_R	0.7
#define MAX_R		5.0

//linear speed (0.2[m/s]-0.8[m/s])
#define MAX_L		0.8
#define MIN_L		0.2
#define INCREMENT_L 0.05
#define STOP_L		0.0
#define FORWARD_L	MAX_L//(MAX_L-MIN_L)/ + MIN_L
#define BACKWARD_L	(MAX_L-MIN_L)/2 + MIN_L


//angular speeds (-0.5[rad/s] - 0.5[rad/s])
#define MAX_A		0.5
#define INCREMENT_A 0.05
#define TURN_SPEED_A  (MAX_A)  //4s for 30°
#define STOP_A			0.0


//commands
#define MAX(a,b) (((a)>(b)) ? a : b)
//#define STOP cmd_.set ( STOP_L, STOP_A);
//#define MOVE_TURN(a,b) cmd_.set ( a, b);
#define FULLCHECK_PERIOD 1

namespace tuw {

/**
 * Robot drives in direction x and its measurements range[0] to range[269]
 * are from robots left side to robots right side.
 * range[0]   is the lase beam distance at angle 2,3562rad
 * range[134] is the lase beam distance at angle 0 rad
 * range[269] is the lase beam distance at angle -2,3562rad
 *
 * To check the wall parallel next tot he robot:
 * from ANGLE_ALPHA grad based on the robots y-axe begins the search area
 * and ends in an angle of  ANGLE_SEARCH_CONUS
 */





/**
 * Robot class
 */
class LocalPlanner {
public:
    enum ControlMode {
        STOP = 0,
        DEMO = 1,
        WANDERER = 2,
        WANDERER2 = 3,
        WALL_FOLLOWING = 4,
        WALL_FOLLOWING2 = 5,
        GOTO = 6,
        GOTO2 = 7
    };
    static std::map<ControlMode, std::string> ControlModeName_; 

    std::chrono::time_point<std::chrono::system_clock> start,end;

    LocalPlanner(const std::string &ns); /// Constructor
    void init();                         /// initialization
    void ai();                           /// artificial intelligence calls a behaviour
    void plot();                         /// plots sensor input

protected:
    enum State_Machine {
            FORWARD = 0,
            RIGHT = 1,
            LEFT = 2,
            BACKWARD = 3,
			STOPPED = 4
        } state;
    Command cmd_;  /// output variables  v, w
    //double linear_speed_;
    //double angular_speed_;
    unsigned long loop_count_; /// counts the filter cycles
    unsigned long counter_;//counter at which timer a full check has to be done
    unsigned int loop_; //registates loops In the states
    bool startTurn;
    MeasurementLaser measurement_laser_;    /// laser measurements

    Figure figure_local_;  /// Figure for data visualization
    void ai_demo();        /// Demo behaviour
    void ai_wanderer();    /// Wanderer behaviour
    void ai_wanderer2();   /// Wanderer behaviour
    void ai_test();//for test purposes
    void plotLocal();      /// plots sensor input in robot coordinates
    tuw_local_planner::LocalPlannerConfig config_;
    bool checkDirection(void);
    bool checkAllAngles(void);
    bool checkRightSide(void);
    bool checkLeftSide(void);

    bool turn_robot_and_stop(double angle);
    void move(const double &linear_speed);
    void drive_back(int seconds);
    void turn(const double &angular_speed);
    void stop();

};

}
#endif // PLANNER_LOCAL_H

