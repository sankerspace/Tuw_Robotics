#include <tuw_self_localization/particle_filter.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/math/distributions/normal.hpp> // for normal_distribution
#include <random>
#include <iostream>
#include <list>


using namespace tuw;

std::random_device ParticleFilter::rd_;
std::mt19937 ParticleFilter::generator_ ( rd_() );
std::uniform_real_distribution<double> ParticleFilter::uniform_distribution_x_;
std::uniform_real_distribution<double> ParticleFilter::uniform_distribution_y_;
std::uniform_real_distribution<double> ParticleFilter::uniform_distribution_theta_;
std::normal_distribution<double> ParticleFilter::normal_distribution_;

ParticleFilter::ParticleFilter() :PoseFilter ( PARTICLE_FILTER ) {
    normal_distribution_ = std::normal_distribution<double> ();
    sigma_likelihood_field_ = 1.0;
}
SamplePtr& ParticleFilter::normal ( SamplePtr &sample, const Pose2D &mean, double sigma_position, double sigma_orientation ) const {
    sample->set ( mean.x() + normal_distribution_ ( generator_ ) * sigma_position, mean.y() + normal_distribution_ ( generator_ ) * sigma_position, mean.theta() + normal_distribution_ ( generator_ ) * sigma_orientation );
    sample->normalizeOrientation();
    return sample;
}

SamplePtr& ParticleFilter::uniform ( SamplePtr &sample, std::uniform_real_distribution<double> distribution_x, std::uniform_real_distribution<double> distribution_y, std::uniform_real_distribution<double> distribution_theta ) const {
    sample->set ( distribution_x ( generator_ ),  distribution_y ( generator_ ),  distribution_theta ( generator_ ) );
    return sample;
}
void ParticleFilter::init ( ) {
    samples.resize ( config_.nr_of_samples );
    switch ( config_.initial_distribution ) {
    case NORMAL_DISTRIBUTION:
        initNormal ();
        break;
    case UNIFORM_DISTRIBUTION:
        initUniform();
        break;
    case GRID_DISTRIBUTION:
        initGrid();
        break;
    default:
        initUniform();
    };
    reset_ = false;
}

void ParticleFilter::initNormal () {
    for ( SamplePtr &s: samples ) {
        s = std::make_shared<Sample>();
        normal ( s, pose_init_, config_.sigma_init_position, config_.sigma_init_orientation );
    }
}

void ParticleFilter::initUniform () {
    for ( SamplePtr &s: samples ) {
        s = std::make_shared<Sample>();
        uniform ( s, uniform_distribution_x_, uniform_distribution_y_, uniform_distribution_theta_ );
    }
}

void ParticleFilter::reinitialize ( const Pose2D &p ) {
    setPoseInit ( p );
    config_.initial_distribution = NORMAL_DISTRIBUTION;
    reset_ = true;
}

void ParticleFilter::initGrid () {
    float angle_division = 16;
    int i = 0;
    double samples_per_angle = config_.nr_of_samples / angle_division;
    double A = ( max_x_ - min_x_ ) * ( max_y_ - min_y_ );
    double samples_per_m2 = samples_per_angle / A ;
    double d =  1.0 / sqrt ( samples_per_m2 );
    samples.clear();
    samples.reserve ( config_.nr_of_samples*2 );
    for ( double x = min_x_ + d/2.; x < max_x_; x+=d ) {
        for ( double y = min_y_ + d/2.; y < max_y_; y+=d ) {
            for ( double theta = -M_PI; theta < M_PI; theta += ( 2.*M_PI ) / angle_division ) {
                samples.push_back ( std::make_shared<Sample>() );
                samples.back()->set ( x,y,theta );
                samples.back()->idx() = i++;
            }
        }
    }
    config_.nr_of_samples = samples.size();

}

double sample_normal_distribution(double b)
{
	double sum=0;
	std::random_device rd;
	  std::mt19937 gen(rd());
	  std::uniform_real_distribution<> distributed(-b, b);


	  for(int i = 0; i <= 12; i++) {
	    sum += distributed(gen);
	  }
	  return sum / 2;
}

void ParticleFilter::update ( const Command &u ) {

    boost::posix_time::time_duration duration = duration_last_update_ + boost::posix_time::millisec ( config_.forward_prediction_time * 1000 );
    double dx, dy, dtheta, dt = duration.total_microseconds() /1000000.;
    double sample_v=0,sample_w=0,sample_g=0;
    double b_v=0,b_w=0,b_g=0;
    double v_=0,w_=0,g_=0,x_=0,y_=0,theta_=0;
    SamplePtr s = std::make_shared<Sample>();
    std::normal_distribution<double> normal_distribution_;

    for ( SamplePtr s : samples ) {
        /**
        * @ToDo MotionModel
        * implement the forward sample_motion_velocity alogrithm and be aware that w can be zero!!
        * use the config_.alpha1 - config_.alpha6 as noise parameters
        **/


#if EXERCISE >= 13


#else

    	b_v=(config_.alpha1*u.v()*u.v()) + (config_.alpha2*u.w()*u.w());
    	b_w=(config_.alpha3*u.v()*u.v()) + (config_.alpha4*u.w()*u.w());
    	b_g=(config_.alpha5*u.v()*u.v()) + (config_.alpha6*u.w()*u.w());


    	//normal_distribution_ = std::normal_distribution<double>(-b_v, b_v);
    	v_=u.v()+sample_normal_distribution(b_v);
    	//normal_distribution_ = std::normal_distribution<double>(-b_w, b_w);
    	w_=u.w()+sample_normal_distribution(b_w);
    	//normal_distribution_ = std::normal_distribution<double>(-b_g, b_g);
    	g_=sample_normal_distribution(b_g);
    	if(w_==0)
    		w_= 0.000001;

    	x_=s->x() - (v_/w_)*sin(s->theta()) + (v_/w_)*sin(s->theta() + w_*dt);
    	y_=s->y() + (v_/w_)*cos(s->theta()) - (v_/w_)*cos(s->theta() + w_*dt);


    	theta_=s->theta() + w_*dt + g_*dt;
    	s->set(x_,y_,theta_);

#endif
    }
}
Pose2D ParticleFilter::localization ( const Command &u, const MeasurementConstPtr &z ) {
    if ( updateTimestamp ( z->stamp() ) ) {
        updateLikelihoodField ();
        if ( reset_ ) init();
        if ( config_.enable_resample ) resample();
        if ( config_.enable_update ) update ( u );
        if ( config_.enable_weighting ) weighting ( ( const MeasurementLaserConstPtr& ) z );
        pose_estimated_ = *samples[0];
    }
    return pose_estimated_;

}
void ParticleFilter::plotData ( Figure &figure_map ) {

    /**
    * @ToDo SensorModel
    * plot the likelihood_field_ into figure_map.background()
    **/


#if EXERCISE >= 20


#else
	//figure_map.background().setTo(cv::Scalar(100,100,0));
		//background.setTo(cv::Scalar(100,100,0));
	cv::Mat& background=figure_map.background();
	float v;
	for ( int r = 0; r < background.rows; r++){//background.rows
		for ( int c = 0; c < background.cols; c++)//background.cols
		{//blau,grün,rot

			//cv::Vec3b color(background.at<cv::Vec3b>(cv::Point(r,c)));
			background.at<cv::Vec3b>(cv::Point(r,c))[0]=255-255*likelihood_field_.at<float>(c, r);
		}

	}

#endif

	double scale =  255.0 / samples_weight_max_ ;
    char text[0xFF];
    for ( int i = samples.size()-1; i >= 0; i-- ) {
        const SamplePtr &s = samples[i];
        /**
        * @ToDo plot samples / particles
        * plot all samples use figure_map.symbol(....
        **/
#if EXERCISE >= 12

#else
        //the samples are sorted in descending order
        // the last n samples with the highest weight are shown

        double w=s->weight()*scale;
        figure_map.symbol(*s,0.1,cv::Scalar(255-w,w,0),1,CV_AA);

#endif
    }
    sprintf ( text, "%4.3fsec", duration_last_update_.total_microseconds() /1000000. );
    cv::putText ( figure_map.view(), text, cv::Point ( figure_map.view().cols-100,20 ), cv::FONT_HERSHEY_PLAIN, 1, Figure::white,3, CV_AA );
    cv::putText ( figure_map.view(), text, cv::Point ( figure_map.view().cols-100,20 ), cv::FONT_HERSHEY_PLAIN, 1, Figure::black,1, CV_AA );

    figure_map.symbol ( pose_estimated_, 0.5, Figure::magenta, 1 );
}

void ParticleFilter::setConfig ( const void *config ) {
    config_ = * ( ( tuw_self_localization::ParticleFilterConfig* ) config );
}
void ParticleFilter::loadMap ( int width_pixel, int height_pixel, double min_x, double max_x, double min_y, double max_y, double roation, const std::string &file ) {
    width_pixel_ = width_pixel,   height_pixel_ = height_pixel;
    min_y_ = min_y, max_y_ = max_y, min_x_ = min_x, max_x_ = max_x, roation_ = roation;
    double dx = max_x_ - min_x_;
    double dy = max_y_ - min_y_;
    double sy = height_pixel / dx;
    double sx = width_pixel  / dy;
    double oy = height_pixel / 2.0;
    double ox = width_pixel  / 2.0;
    double ca = cos ( roation ), sa = sin ( roation );
    if ( sy == sx ) scale_ = sy;
    else {
        std::cerr << "loadMap: nonsymetric scale!";
        return;
    }
    double owx = min_x_ + dx/2.;
    double owy = min_y_ + dy/2.;
    cv::Matx<double, 3, 3 > Tw ( 1, 0, -owx, 0, 1, -owy, 0, 0, 1 ); // translation
    cv::Matx<double, 3, 3 > Sc ( sx, 0, 0, 0, sy, 0, 0, 0, 1 ); // scaling
    cv::Matx<double, 3, 3 > Sp ( -1, 0, 0, 0, 1, 0, 0, 0, 1 );  // mirroring
    cv::Matx<double, 3, 3 > R ( ca, -sa, 0, sa, ca, 0, 0, 0, 1 ); // rotation
    cv::Matx<double, 3, 3 > Tm ( 1, 0, ox, 0, 1, oy, 0, 0, 1 ); // translation
    tf_ = Tm * R * Sp * Sc * Tw;

    map_.create ( height_pixel_, width_pixel_ );
    distance_field_pixel_.create ( height_pixel_, width_pixel_ );
    likelihood_field_.create ( height_pixel_, width_pixel_ );
    cv::Mat image = cv::imread ( file, CV_LOAD_IMAGE_GRAYSCALE );
    cv::resize ( image, map_, cv::Size ( map_.cols, map_.rows ), cv::INTER_AREA );

    uniform_distribution_x_ =  std::uniform_real_distribution<double> ( min_x_, max_x_ );
    uniform_distribution_y_ = std::uniform_real_distribution<double> ( min_y_, max_y_ );
    uniform_distribution_theta_ = std::uniform_real_distribution<double> ( -M_PI, M_PI );

    updateLikelihoodField ();
}
void ParticleFilter::updateLikelihoodField () {
	//static int counter_=0;
    if ( sigma_likelihood_field_ == config_.sigma_hit ) return;
    sigma_likelihood_field_ = config_.sigma_hit;
    boost::math::normal normal_likelihood_field = boost::math::normal ( 0, config_.sigma_hit );

    /**
    * @ToDo Computing the Likelihood Field
    * using the cv::distanceTransform and the boost::math::pdf
    **/
#if EXERCISE >= 21
#else


    cv::distanceTransform(map_,distance_field_pixel_,CV_DIST_L2,3);
    //cv::distanceTransform(map_,distance_,CV_DIST_L2,3);

    for ( int r = 0; r < likelihood_field_.rows; r++ ) {
        for ( int c = 0; c < likelihood_field_.cols; c++ ) {
        	float value=distance_field_pixel_.at<float>(r,c)/scale_;
        	distance_field_pixel_.at<float>(r,c)=value;
        	likelihood_field_ ( r,c ) = (float)boost::math::pdf(normal_likelihood_field, value);
        	//distance_field_pixel_(r,c) = distance_.at<float>(cv::Point(c,r))/scale_;
            //likelihood_field_ ( r,c ) = boost::math::pdf(normal_likelihood_field, distance_field_pixel_.at<float>(cv::Point(c,r)));
        }
    }
#endif
    //counter_++;
}

void ParticleFilter::weighting ( const MeasurementLaserConstPtr &z ) {
	static bool switch_=config_.random_beams;
	uint8_t beam_increment=0;
    if ( config_.nr_of_beams >  z->size() ) config_.nr_of_beams = z->size();
    std::vector<size_t> used_beams( config_.nr_of_beams ); /// vector of beam indexes used
    beam_increment=z->size()/config_.nr_of_beams;


    /**
    * @ToDo Select beams for weighting
    **/
#if EXERCISE >= 23
#else

    /// Dummy: fills all values with the same value
    for ( size_t i = 0; i < used_beams.size(); i++ ) {
    	if(!switch_)
    	{
    		used_beams[i] = (i+1)*beam_increment;
    		//printf("used beams index[#%d]:%d\n",config_.nr_of_beams,used_beams[i]);
    	}else{

    		used_beams[i] =rand() % 5 + floor(i*((z->size()-5)/used_beams.size())); //from 1 to 270
    		//printf("used beams index[#%d]:%d\n",config_.nr_of_beams,used_beams[i]);
    	}
    }
#endif

    /**
    * @ToDo Computing the Weight
    * the used_beams should define the index of used laser beams
    **/
#if EXERCISE >= 22
#else
    //#############################################################################
    Point2D point,diff;
    double q=0,const_=(config_.z_rand/config_.z_max);


    /// Dummy: computes a funny weight
  for ( size_t idx = 0; idx < samples.size(); idx++ ) {
        SamplePtr &s = samples[idx];

        //double w = sqrt ( s->x() *s->x() +s->y() *s->y() );
       // s->weight() = sqrt ( s->x() *s->x() +s->y() *s->y() );
       q=1;
        for (int i=0;i<used_beams.size();i++)
        {
        	point=(*z)[used_beams[i]].end_point;

        	if( (*z)[used_beams[i]].length < z->range_max() && (*z)[used_beams[i]].length > z->range_min()){

        		point= tf_  * s->tf() * z->tf() * point;

        		//check the bounderies of the calulated points, stay inside the area
        		if((point.x()>0 && point.x()<width_pixel_)&&(point.y()> 0 && point.y() < height_pixel_)){

        			//printf("x: %f , y: %f\n",point.x(),point.y());
        			//q=q*(config_.z_hit * likelihood_field_((int)point.y(),(int)point.x())+const_);
        			q=q*(config_.z_hit * likelihood_field_((int)point.y(),(int)point.x())+const_);
        		} else{
        			q=q*const_;
        		}
        	}else continue;

        	s->weight() = q;
        }
    }

   //###############################################################################
#endif

    std::sort ( samples.begin(),  samples.end(), Sample::greater );

    double samples_weight_sum = 0;
    for ( const SamplePtr &s: samples ) {
        samples_weight_sum += s->weight();
    }
    samples_weight_max_ = 0;
    for ( size_t i = 0; i < samples.size(); i++ ) {
        SamplePtr &s = samples[i];
        s->weight() /= samples_weight_sum;
        s->idx() = i;
        if ( samples_weight_max_ < s->weight() ) samples_weight_max_ = s->weight();
        //std::cout << s->idx() << ": " << s->weight() << std::endl;
    }
}

void ParticleFilter::resample () {
	static bool first_run=true;
	static bool change_resample=false;
    double dt = duration_last_update_.total_microseconds() /1000000.;
    std::uniform_real_distribution<double> d ( 0,1 );
    std::uniform_int_distribution<size_t>  uniform_idx_des ( 0,samples.size()-1 );
    /**
    * @ToDo Resample
    * implement a resample weel
    **/
#if EXERCISE >= 31
#else
    double sum=0;
    size_t particle_size_index=samples.size()-1;

    if(first_run)
    {
    	if(!config_.resample_switch)
    		if(first_run)
    			printf("\n\n################Resample strategy is Cloning.####################\n\n");
    	else
    		if(first_run)
    			printf("\n\n################Resample strategy is Low Variance Resampling.######\n\n");
    	first_run=false;
    }else{
		if(!config_.resample_switch)
		{//remove the M lowest weighted particles and clone the M weightest ones
			if(change_resample && !config_.resample_switch){
				printf("Changed to Cloning strategy.\n");
				change_resample=false;
			}
			size_t M=config_.resample_rate*samples.size();
			if((M>=1)){
				int index=0;
				while(index<M){
					SamplePtr &s_high_weighted = samples[index];
					SamplePtr &s_low_weighted = samples[particle_size_index-index];
					//set low weighted sample with the gaussian distributed positioned sample with mean from high weighted sample
					normal(s_low_weighted, *s_high_weighted,config_.sigma_static_position , config_.sigma_static_orientation);
					//s_low_weighted->set(s_low_weighted->position(),s_high_weighted->weight());
					index++;
				}
			}
		}else
		{//The low variance sampling
			if(!change_resample && config_.resample_switch){
				printf("Changed to Low variance strategy.\n");
				change_resample=true;
			}
			size_t M=floor(config_.resample_rate*samples.size());
			double r=1.0 / (rand() % M);
			double c=samples[0]->weight();//first sample
			double U=0;
			double M_inv=(1.0/M);
			int i=0,j=0,sum=0;
			//std::vector< SamplePtr > samples_new;

			//double boundary=samples[0]->weight()/partitions;
			//double interval_max=samples[0]->weight();
		    //double interval_min=interval_max-boundary;
			for(double m=1.0;m<=M;m++)
			{	//according algorithm in [Thrun] p110.
				U=r+(m-1)*M_inv;
				while(U>c && (i+1)<samples.size())
				{
					i=i+1;
					c=c+samples[i]->weight();

				}
				//if(j<M){
					SamplePtr s = samples[uniform_idx_des(generator_)];
					normal(s, *samples[i],config_.sigma_static_position , config_.sigma_static_orientation);
					//j++;
				//}//if
			}//for
		}//else
    }//if firstsrun else


   // ROS_INFO("Resample finished.");


#endif
    /// update number of samples
    if ( config_.nr_of_samples < samples.size() ) samples.resize ( config_.nr_of_samples );
    while ( config_.nr_of_samples > samples.size() ) {
        SamplePtr &parent = samples[uniform_idx_des ( generator_ )];
        double p = d ( generator_ );
        samples.push_back ( std::make_shared<Sample> ( *parent ) );
        SamplePtr &s  = samples.back();
        normal ( s, *s, config_.sigma_static_position*dt, config_.sigma_static_orientation*dt );
    }
}
