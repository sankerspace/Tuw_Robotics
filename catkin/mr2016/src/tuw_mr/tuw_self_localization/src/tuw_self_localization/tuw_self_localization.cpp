#include "tuw_self_localization/tuw_self_localization.h"
#include <opencv/cv.h>
#include <boost/concept_check.hpp>

using namespace tuw;

void SelfLocalization::onMouseMap ( int event, int x, int y, int flags, void* param ) {
    SelfLocalization *self_localization = ( SelfLocalization * ) param;
    self_localization->mouse_on_map_ =  self_localization->figure_map_.m2w ( Point2D ( x,y ) );
    static Pose2D pose;

    /**
     * @ToDo React on mouse clicks
     * catch the mouse and reset the filter
     * use the event CV_EVENT_LBUTTONDOWN and CV_EVENT_LBUTTONUP to define a pose with orientation
     * reset the filter with ground truth on the middle button
     **/
#if EXERCISE >= 30
#else
    if ( event == CV_EVENT_LBUTTONDOWN  ) {
    	std::cout <<"Position[]:"<< self_localization->mouse_on_map_ << std::endl;
    	pose.position()=self_localization->mouse_on_map_;


    }
    if ( event == CV_EVENT_LBUTTONUP ) {
        	std::cout <<"Position[]:"<< self_localization->mouse_on_map_ << std::endl;
        	pose.set(pose.position(),self_localization->mouse_on_map_);
        	self_localization->pose_filter_->reinitialize(pose);

        }
    if ( event == CV_EVENT_MBUTTONDOWN ) {
    	std::cout <<"Middle Button Event."<< std::endl;
    	pose.set(self_localization->pose_ground_truth_);
    	//self_localization->pose_filter_->reinitialize(pose);
    	//self_localization->pose_filter_->setPoseInit(pose);
    	self_localization->pose_filter_->reinitialize(pose);
    }


    if ( event == CV_EVENT_RBUTTONDOWN ) {

    	//self_localization->config_.Iinitial_distribution = UNIFORM_DISTRIBUTION;
    	//self_localization->pose_estimated_
/*
    	switch(self_localization->pose_filter_->getType()){
    	case tuw::PoseFilter::PARTICLE_FILTER:
    		//std::shared_ptr< ParticleFilter > filter= (std::shared_ptr< ParticleFilter >)self_localization->pose_filter_;//std::make_shared<ParticleFilter>()
    		ParticleFilterPtr filter= std::dynamic_pointer_cast<ParticleFilterPtr>(self_localization->pose_filter_);
    		//filter->config_.initial_distribution=UNIFORM_DISTRIBUTION;
    	break;

    	case tuw::PoseFilter::KALMAN_FILTER:

    		//std::shared_ptr< KalmanFilter > filter= (std::shared_ptr< KalmanFilter >)self_localization->pose_filter_;
    		//filter->config_.initial_distribution=UNIFORM_DISTRIBUTION;
    		break;
    	}
*/
    }
#endif
}

SelfLocalization::SelfLocalization ( const std::string &ns )
    : loop_count_ ( 0 )
    , figure_map_ ( ns + ", Global View" ) {
    measurement_laser_ = std::make_shared<tuw::MeasurementLaser>();   /// laser measurements
}

void SelfLocalization::init() {
    figure_map_.init ( config_.map_pix_x, config_.map_pix_y,
                       config_.map_min_x, config_.map_max_x,
                       config_.map_min_y, config_.map_max_y,
                       config_.map_rotation + M_PI,
                       config_.map_grid_x, config_.map_grid_y, filename_map_image_ );

    figure_map_.setLabel ( "x=%4.2f","y=%4.2f" );

    if ( config_.plot_data ) cv::namedWindow ( figure_map_.title(), 1 );
    if ( config_.plot_data ) cv::setMouseCallback ( figure_map_.title(), SelfLocalization::onMouseMap, this );
    if ( config_.plot_data ) {
        cv::moveWindow ( figure_map_.title(), 20, 20 );
    }


    std::string filename_map;
    if ( pose_filter_->getType() == PoseFilter::PARTICLE_FILTER ) filename_map =  filename_map_image_;
    if ( pose_filter_->getType() == PoseFilter::KALMAN_FILTER ) filename_map =  filename_map_lines_;

    pose_filter_->loadMap ( config_.map_pix_x, config_.map_pix_y,
                            config_.map_min_x, config_.map_max_x,
                            config_.map_min_y, config_.map_max_y,
                            config_.map_rotation + M_PI, filename_map );
							//config_.map_rotation, filename_map );
}


void SelfLocalization::plot() {
    if ( config_.plot_data ) plotMap();
    cv::waitKey ( 10 );
}

void SelfLocalization::plotMap() {
    figure_map_.clear();
    char text[0xFF];

    cv::Matx33d M = pose_ground_truth_.tf();


    for ( size_t i = 0; i < measurement_laser_->size(); i++ ) {
        /**
        * @ToDo plot sensor data
        * plot the laser data into the map and use the transformation measurement_laser_->tf() as well!!
        **/
#if EXERCISE >= 11


#else

    	Point2D pm ( (*measurement_laser_)[i].end_point.x(),(*measurement_laser_)[i].end_point.y());
    	//pm=(*measurement_laser_).tf()*pm;
    	//measurement_laser_->tf()=M* measurement_laser_->tf();
    	figure_map_.symbol (M*measurement_laser_->tf()*pm, Figure::red );
    	//Point2D pm ( cos ( i ),sin ( i ) );
        //figure_map_.symbol ( pm, Figure::red );
#endif

    }
    sprintf ( text, "%5lu,  <%+4.2fm, %+4.2f>", loop_count_, mouse_on_map_.x(), mouse_on_map_.y() );
    cv::putText ( figure_map_.view(), text, cv::Point ( 20,20 ), cv::FONT_HERSHEY_PLAIN, 1, Figure::white,3, CV_AA );
    cv::putText ( figure_map_.view(), text, cv::Point ( 20,20 ), cv::FONT_HERSHEY_PLAIN, 1, Figure::black,1, CV_AA );
    /**
    * @ToDo Put your name into the figure
    **/
    cv::putText ( figure_map_.view(), "Marko Stanisic",
                      cv::Point ( figure_map_.view().cols-250, figure_map_.view().rows-10 ),
                      cv::FONT_HERSHEY_PLAIN, 1, Figure::black, 1, CV_AA );
    /** END ToDo **/
    figure_map_.symbol ( odom_, 0.2, Figure::cyan, 1 );
    figure_map_.symbol ( pose_ground_truth_, 0.2, Figure::orange, 1 );
    pose_filter_->plotData ( figure_map_ );
    cv::imshow ( figure_map_.title(),figure_map_.view() );
    cv::waitKey ( 10 );
}

void SelfLocalization::localization () {
    if ( measurement_laser_->empty() ) return;
    pose_estimated_ = pose_filter_->localization ( cmd_, ( MeasurementPtr ) measurement_laser_ );
    loop_count_++;
}

