#include "tuw_self_localization/kalman_filter.h"
#include <boost/lexical_cast.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <tuw_geometry/linesegment2d_detector.h>
#include <iostream>
using namespace tuw;
KalmanFilter::KalmanFilter()
    : PoseFilter ( KALMAN_FILTER )
    , figure_hspace_ ( "Hough Space" ) {
}

void KalmanFilter::init ( ) {
    pose_estimated_ = pose_init_;
    P = cv::Matx<double, 3, 3> ( config_.init_sigma_location, 0, 0,
                                 0, config_.init_sigma_location, 0,
                                 0, 0, config_.init_sigma_orientation );
    reset_ = false;


}

void KalmanFilter::detect_lines ( const MeasurementLaserConstPtr &z ) {

    LineSegment2DDetector linesegment_detector;
    linesegment_detector.config_.threshold_split_neighbor = config_.line_dection_split_neighbor;
    linesegment_detector.config_.threshold_split = config_.line_dection_split_threshold;
    linesegment_detector.config_.min_length = config_.line_dection_min_length;
    linesegment_detector.config_.min_points_per_line = config_.line_dection_min_points_per_line;
    linesegment_detector.config_.min_points_per_unit = config_.line_dection_min_points_per_unit;
    measurement_local_scanpoints_.resize ( z->size() );
    for ( size_t i = 0; i < z->size(); i++ ) {
        measurement_local_scanpoints_[i] = z->tf() * z->operator[] ( i ).end_point;
    }
    measurement_linesegments_.clear();
    linesegment_detector.start ( measurement_local_scanpoints_, measurement_linesegments_ );

    measurement_match_.resize ( measurement_linesegments_.size(), -1 );
}


void KalmanFilter::plotData ( Figure &figure_map ) {
    plotMap ( figure_map );
    if ( config_.plot_hough_space ) plotHoughSpace();
}

void KalmanFilter::plotMap ( Figure &figure_map ) {
    char text[0xFF];
    cv::Scalar color;

    /// Plot known line segments (map)
    for ( size_t i = 0; i < map_linesegments_.size(); i++ ) {
        color = Figure::orange;
        figure_map.line ( map_linesegments_[i].p0(), map_linesegments_[i].p1(), color, 1 );

      figure_map.putText ( boost::lexical_cast<std::string> ( i ),  map_linesegments_[i].pc(), cv::FONT_HERSHEY_PLAIN, 0.6, Figure::white,  3, CV_AA );
      figure_map.putText ( boost::lexical_cast<std::string> ( i ),  map_linesegments_[i].pc(), cv::FONT_HERSHEY_PLAIN, 0.6, color, 1, CV_AA );
    }
    cv::Matx33d M = pose_estimated_.tf();
    for ( size_t i = 0; i < measurement_linesegments_.size(); i++ ) {
        color = Figure::red;
        if ( measurement_match_[i] >= 0 ) color = Figure::green_dark;

        /**
        * @ToDo visualize the measurment
        **/
#if EXERCISE >= 40
       
#else
        LineSegment2D segment=measurement_linesegments_[i];
        Point2D p0 =M*segment.p0();
        Point2D p1 =M*segment.p1();
        Point2D pc =M*segment.pc();

        figure_map.line ( p0, p1, color );
        figure_map.putText ( boost::lexical_cast<std::string> ( i ),  pc, cv::FONT_HERSHEY_PLAIN, 0.6, Figure::white,  3, CV_AA );
        figure_map.putText ( boost::lexical_cast<std::string> ( i ),  pc, cv::FONT_HERSHEY_PLAIN, 0.6, color, 1, CV_AA );


#endif

    }

    for ( size_t i = 0; i < measurement_match_.size(); i++ ) {
        if ( measurement_match_[i] >= 0 ) {
            /**
            * @ToDo visualize the matches
            * it is up to you how you visualize the realtion
            **/
#if EXERCISE >= 40

#else
        	LineSegment2D segment=map_linesegments_[measurement_match_[i]];
        	Point2D p0_0=segment.p0(),p0_1;
        	Point2D p1_0=segment.p1(),p1_1;
        	double a=segment.a(),b=segment.b(),c=segment.c();
        	double k_line =-(a/b),k_normal=1/k_line;
        	double delta_x=sqrt((0.5*0.5)/(k_normal));
        	double delta_y=k_normal*delta_x+(c/b);
        	LineSegment2D parallel;
        	if(a>-0.5 && a<0.5) //horizontal line a=0 but with tolerance +-0.5
        	{
        		p0_1.set(p0_0.x(),p0_0.y()+0.5);
        		p1_1.set(p1_0.x(),p1_0.y()+0.5);
        		parallel.set(p0_0.x(),p0_0.y()+0.25,p1_0.x(),p1_0.y()+0.25);

        	}else if(b>-0.5 && b<0.5)//vertical line b=0 but with tolerance +-0.5
        	{
        		p0_1.set(p0_0.x()+0.5,p0_0.y());
        		p1_1.set(p1_0.x()+0.5,p1_0.y());
        		parallel.set(p0_0.x()+0.25,p0_0.y(),p1_0.x()+0.25,p1_0.y());

        	}else if(k_line>0)// line with positive angle
        	{
           		p0_1.set(p0_0.x()+a,p0_0.y()+b);
           		p1_1.set(p1_0.x()+a,p1_0.y()+b);
           		parallel.set(p0_0.x()+(a/2),p0_0.y()+(b/2),p1_0.x()+(a/2),p1_0.y()+(b/2));
        	}else if(k_line<0)//line with negative angle
        	{
           		p0_1.set(p0_0.x()+a,p0_0.y()+b);
           		p1_1.set(p1_0.x()+a,p1_0.y()+b);
           		parallel.set(p0_0.x()+(a/2),p0_0.y()+(b/2),p1_0.x()+(a/2),p1_0.y()+(b/2));
        	}

        	//printf("line[%d]:%f,%f to %f,%f\n",i,p0_.x(),p0_.y(),p1_.x(),p1_.y());
        	figure_map.line ( p0_0,p0_1, Figure::blue_dark);
        	figure_map.line ( p1_0,p1_1, Figure::blue_dark);

        	figure_map.line ( parallel.p0(),parallel.p1(), Figure::blue_dark);
        	//p0_=map_linesegments_[i].p1();
        	std::stringstream s;
        	s<<std::setprecision(2)<<parallel.length()<<"m";

        	 figure_map.putText (s.str() ,  parallel.pc(), cv::FONT_HERSHEY_PLAIN, 0.6, Figure::white,  3, CV_AA );
        	 figure_map.putText (s.str(),   parallel.pc(), cv::FONT_HERSHEY_PLAIN, 0.6, color, 1, CV_AA );


#endif

        }
    }


    sprintf ( text, "%4.3fsec", duration_last_update_.total_microseconds() /1000000. );
    cv::putText ( figure_map.view(), text, cv::Point ( figure_map.view().cols-100,20 ), cv::FONT_HERSHEY_PLAIN, 1, Figure::white,3, CV_AA );
    cv::putText ( figure_map.view(), text, cv::Point ( figure_map.view().cols-100,20 ), cv::FONT_HERSHEY_PLAIN, 1, Figure::black,1, CV_AA );

    /**
    * @ToDo visualize the pose coaraiance
    * Compute and plot the pose coaraiance in x and y direction
    * take the pose covariance P and crate a 2x2 matrix out of the x,y components
    * transform the matrix into the plot E = Mw2m*P(0:1,0:1)*Mw2m'
    * use the opencv to compute eigenvalues and eigen vectors to compute the size and orienation of the ellipse
    **/
#if EXERCISE >= 40
  
#else
    cv::Matx<double, 2, 2> E ( 0,0,0,0);  /// must be changed
    cv::Matx<double, 2, 2> Mw2m_=figure_map.Mw2m().get_minor<2,2>(0,0);
    cv::Matx<double, 2, 2> P_= P.get_minor<2,2>(0,0);
    E= Mw2m_* P_ * Mw2m_.t();
    cv::Mat_<double> eigval, eigvec;
    cv::eigen ( E, eigval, eigvec );
    cv::RotatedRect ellipse ( ( figure_map.Mw2m() * pose_estimated_.position() ).cv(),cv::Size ( 2*sqrt(eigval(0)),2*sqrt(eigval(1)) ), atan2(eigvec(0,1),eigvec(0,0))); /// must be changed
    cv::ellipse ( figure_map.view(),ellipse, Figure::magenta, 1, CV_AA );
#endif

    for ( size_t i = 0; i < msgs_.size(); i++ ) {
        cv::putText ( figure_map.view(), msgs_[i].c_str(), cv::Point ( 10,figure_map.view().rows - 12* ( i+1 ) ), cv::FONT_HERSHEY_PLAIN, 0.6, Figure::white,3, CV_AA );
        cv::putText ( figure_map.view(), msgs_[i].c_str(), cv::Point ( 10,figure_map.view().rows - 12* ( i+1 ) ), cv::FONT_HERSHEY_PLAIN, 0.6, Figure::black,1, CV_AA );
    }

    ///Plot estimated pose
    figure_map.symbol ( pose_estimated_, 0.5, Figure::magenta, 1 );
}
void KalmanFilter::plotHoughSpace ( ) {

    if ( figure_hspace_.initialized() == false ) {
        figure_hspace_.setLabel ( "alpha=%4.2f","rho=%4.2f" );
       // void init ( int width_pixel, int height_pixel,
        //double min_y,double max_y,
		//double min_x, double max_x,
		//double rotation = 0,
		//double grid_scale_x = -1,
		//double grid_scale_y = -1,
		//const std::string &background_image = std::string() );

        figure_hspace_.init ( config_.hough_space_pixel_alpha, config_.hough_space_pixel_rho,
                              -M_PI*1.1, +M_PI*1.1,
                              0, config_.hough_space_meter_rho,
                              M_PI,
                              1, M_PI/4 );
        /*printf("\n\nHspace-Figure:\n");
        printf("min_x:%f max_x:%f min_y:%f max_y:%f scale_x:%f scale_y:%f\n\n",figure_hspace_.min_x(),
        		figure_hspace_.max_x(),figure_hspace_.min_y(),figure_hspace_.max_y(),
				figure_hspace_.scale_x(),figure_hspace_.scale_y());
		*/
        if ( config_.plot_hough_space ) cv::namedWindow ( figure_hspace_.title(), 1 );
        if ( config_.plot_hough_space ) {
            cv::moveWindow ( figure_hspace_.title(), 640, 20 );
        }
    }
    figure_hspace_.clear();

    cv::Rect rectSpace ( 0,0, figure_hspace_.view().cols, figure_hspace_.view().rows );
    for ( unsigned int i = 0; i < measurement_local_scanpoints_.size(); i++ ) {
        Point2D p0 = measurement_local_scanpoints_[i];
        for ( double alpha = figure_hspace_.min_x(); alpha < figure_hspace_.max_x(); alpha +=1.0/figure_hspace_.scale_x() ) {
            /**
            * @ToDo EKF
            * draw a wave with angle = [-pi...pi], r = x*cos(angle) + y *sin(angle) for every laser point [x,y].
            * The function Line2D::toPolar() can be used to transform a line into polar coordinates
            **/
#if EXERCISE >= 44
         
#else

        	double rho_=0,alpha_=0;
        	rho_=cos(alpha)*p0.x()+sin(alpha)*p0.y();

        	//manual version
/*
        	alpha_=(alpha-figure_hspace_.min_x())*figure_hspace_.scale_x();
        	rho_=rho_* figure_hspace_.scale_y();
        	cv::Point hspace=cv::Point(alpha_,rho_);
*/

        	//automatic version

        	Point2D p_hugh=figure_hspace_.w2m(Polar2D(alpha,rho_)) ;
        	cv::Point hspace=p_hugh.cv();



            if ( hspace.inside ( rectSpace ) ) {
                figure_hspace_.view().at<cv::Vec3b> ( hspace ) -=  cv::Vec3b ( 50,10,10 );
            }
#endif
        }
    }


    cv::Scalar color;
    Tf2D tf = figure_hspace_.Mw2m();
    for ( size_t i = 0; i < predicted_linesegments_.size(); i++ ) {
        color = Figure::orange;
        /**
        * @ToDo Plot measurement prediction
        * the map prediction in the hough space as a circle or dot
        **/
#if EXERCISE >= 44       
#else
        //double alpha_=0,rho_=0;
        Line2D line_=predicted_linesegments_[i];
        //manual version
/*

        alpha_=(line_.toPolar().alpha() -figure_hspace_.min_x())*figure_hspace_.scale_x();
        rho_=line_.toPolar().rho() * figure_hspace_.scale_y();
        Polar2D polar(alpha_,rho_);
*/
        //automatic version

        Polar2D polar = line_.toPolar();//ohne Transformation!!!!!???????????ß

        figure_hspace_.circle ( polar, 5, color, 1 );
        figure_hspace_.putText ( boost::lexical_cast<std::string> ( i ),  polar, cv::FONT_HERSHEY_PLAIN, 0.6, Figure::white,  3, CV_AA );
        figure_hspace_.putText ( boost::lexical_cast<std::string> ( i ),  polar, cv::FONT_HERSHEY_PLAIN, 0.6, color, 1, CV_AA );
#endif
    }
    cv::RotatedRect ellipse;
    ellipse.angle  = 0;
    ellipse.size.width  = config_.data_association_line_alpha  * figure_hspace_.scale_x() * 2.0;
    ellipse.size.height = config_.data_association_line_rho * figure_hspace_.scale_y() * 2.0;
    for ( size_t i = 0; i < measurement_linesegments_.size(); i++ ) {
        Polar2D  polar = measurement_linesegments_[i].toPolar();
        color = Figure::blue_dark;
        /**
         * @ToDo Plot measurement
         * Plot the measurement prediction KalmanFilter::predicted_linesegments_ with an ellipse
         * to show the data association threshold config_.data_association_line_alpha and config_.data_association_line_rho.
         **/
#if EXERCISE >= 44     
#else
        //manual version
/*
        double alpha_=0,rho_=0;
        alpha_=(polar.alpha()-figure_hspace_.min_x())*figure_hspace_.scale_x();
        rho_=polar.rho()* figure_hspace_.scale_y();
        //polar=Polar2D(alpha_,rho_);
        ellipse.center =cv::Point2f(alpha_,rho_);
        polar.rho()=figure_hspace_.min_y()-polar.rho()+figure_hspace_.max_y();////
*/
        //automatic version

         //polar=tf * polar;
        //polar=figure_hspace_.w2m(polar);
         //ellipse.center =polar.point().cv();//cv::Point2f(polar.alpha(),polar.rho());
         ellipse.center = (figure_hspace_.Mw2m()*polar).cv();
        cv::ellipse ( figure_hspace_.view(), ellipse, color, 1, CV_AA );

        figure_hspace_.putText ( boost::lexical_cast<std::string> ( i ),  polar, cv::FONT_HERSHEY_PLAIN, 0.6, Figure::white,  3, CV_AA );
        figure_hspace_.putText ( boost::lexical_cast<std::string> ( i ),  polar, cv::FONT_HERSHEY_PLAIN, 0.6, color, 1, CV_AA );
#endif
    }
    cv::imshow ( figure_hspace_.title(),figure_hspace_.view() );
}

void KalmanFilter::data_association ( ) {
    if ( config_.enable_data_association == false ) return;

    predicted_linesegments_.resize ( map_linesegments_.size() );
    Tf2D M = pose_predicted_.tf().inv();
    for ( size_t i = 0; i < predicted_linesegments_.size(); i++ ) {
        /**
        * @ToDo compute the mesurment prediction
        * predicted_linesegments_[i].set ( .... )
        **/
#if EXERCISE >= 43    
#else

    	LineSegment2D line_=map_linesegments_[i];
    	Point2D pointA,pointB;
    	pointA=M * line_.p0();
    	pointB=M * line_.p1();
    	/*printf("WPointA:[%f,%f] WPointB:[%f,%f] ,SPointA:[%f,%f] ,SPointB:[%f,%f] \n",map_linesegments_[i].x0(),
    			map_linesegments_[i].y0(),map_linesegments_[i].x1(),map_linesegments_[i].y1(),pointA.x(),pointA.y(),pointB.x(),pointB.y());
        */
        predicted_linesegments_[i].set(pointA,pointB);



#endif
    }

    /// Match line segments in polar coordinates which are near to the robot
    Tf2D Mp2h = figure_hspace_.Mw2m();//??????????????????????????????????????????????????????????ßß
    for ( size_t i = 0; i < measurement_linesegments_.size(); i++ ) {
        Polar2D measurement = measurement_linesegments_[i].toPolar();
        float dMin = FLT_MAX;
        double p_delta_distance=0,p_delta_angle=0,normal_distance_d0=0,normal_distance_d1=0,normal_distance=0;
        measurement_match_[i] = -1;
        for ( size_t j = 0; j < predicted_linesegments_.size(); j++ ) {
            Polar2D prediction = predicted_linesegments_[j].toPolar();
            /**
            * @ToDo matching mesurment with prediction
            * find the best mesurment prediction idx j and store it in measurement_match_[i]
            **/
#if EXERCISE >= 43           
#else
            double limit_rho;
            limit_rho=config_.data_association_line_rho;
            double limit_alpha;
            limit_alpha=config_.data_association_line_alpha;
            p_delta_distance=abs(measurement.rho()-prediction.rho());
            p_delta_angle= angle_difference(measurement.alpha(), prediction.alpha()) ;
            //p_delta_angle=fabs( p_delta_angle );//

            normal_distance_d0=predicted_linesegments_[j].distanceTo(measurement_linesegments_[i].p0());
            normal_distance_d1=predicted_linesegments_[j].distanceTo(measurement_linesegments_[i].p1());
            normal_distance=sqrt(pow(normal_distance_d0,2)+pow(normal_distance_d1,2));

            if(normal_distance<=config_.data_association_distance_to_endpoints){
            	if((p_delta_distance>=-limit_rho && p_delta_distance<=limit_rho) &&
            			(p_delta_angle>=-limit_alpha && p_delta_angle<=limit_alpha)){
            		//printf("data_rho:%f,data_alpha:%f,delta_r:%f,delta_ang:%f\n", limit_rho,
            			//	limit_alpha,p_delta_distance,p_delta_angle);

            		measurement_match_[i]=j;
            		//printf("Match Line map:%d to measure:%d\n ",j,i);
            	}

            }else
            	continue;
#endif
        }
    }


}

void KalmanFilter::reinitialize ( const Pose2D &p ) {
    setPoseInit ( p );
    reset();
}


void KalmanFilter::loadMap ( int width_pixel, int height_pixel, double min_y, double max_y, double min_x, double max_x, double roation, const std::string &file ) {
    init();
    cv::FileStorage fs ( file, cv::FileStorage::READ );
    cv::Mat_<double> l;
    fs["line segments"] >> l;
    map_linesegments_.resize ( l.rows );
    for ( size_t i = 0; i < map_linesegments_.size(); i++ ) {
        map_linesegments_[i].set ( l ( i,0 ),l ( i,1 ), l ( i,2 ),l ( i,3 ) );
    }
}

Pose2D KalmanFilter::localization ( const Command &u, const MeasurementConstPtr &z ) {
    detect_lines ( ( const MeasurementLaserConstPtr& ) z );
    if ( updateTimestamp ( z->stamp() ) ) {
        if ( reset_ ) init();
        prediction ( u );
        data_association ( );
        correction ( );
    }
    return pose_estimated_;
}

void KalmanFilter::setConfig ( const void *config ) {
    config_ = * ( ( tuw_self_localization::KalmanFilterConfig* ) config );
}

void KalmanFilter::prediction ( const Command &u ) {
    x = pose_estimated_.state_vector();
    if ( config_.enable_prediction ) {

        /**
        * @ToDo predicte pose and covariance
        * compute KalmanFilter::xp and KalmanFilter::Pp as predicted pose and Covariance
        **/
#if EXERCISE >= 41      
#else
    	boost::posix_time::time_duration duration = duration_last_update_ + boost::posix_time::millisec ( config_.forward_prediction_time * 1000 );
    	double w_=u.w(),v_=u.v(),dt = duration.total_microseconds() /1000000.;
    	double x_=x[0],y_=x[1],theta_=x[2];
    	double dx=0,dy=0,dx_dtheta_G=0,dy_dtheta_G=0,d;
    	double dx_dv_V=0,dx_dw_V=0,dy_dv_V=0,dy_dw_V=0,dtheta_dv_V=0,dtheta_dw_V=0;

    	if(w_!=0){//no division with zero
			dy_dtheta_G= -(v_/w_)*sin(theta_) + (v_/w_)*sin(theta_ + w_*dt);
			dx_dtheta_G= -(v_/w_)*cos(theta_) + (v_/w_)*cos(theta_ + w_*dt);


			dx_dv_V=(-sin(theta_)+sin(theta_+w_*dt))/w_;
			dx_dw_V=( (v_* (sin(theta_)-sin(theta_+w_*dt)))/(w_*w_) ) + ( (v_*cos(theta_+w_*dt)*dt)/w_ );
			dy_dv_V=(cos(theta_)-cos(theta_+w_*dt))/w_;
			dy_dw_V=-( (v_* (cos(theta_)-cos(theta_+w_*dt)))/(w_*w_) ) + ( (v_*sin(theta_+w_*dt)*dt)/w_ );
			dtheta_dv_V=0;
			dtheta_dw_V=dt;

			dx=dy_dtheta_G;
			dy=(v_/w_)*cos(theta_) - (v_/w_)*cos(theta_ + w_*dt);
    	}else{
    		dy_dtheta_G= v_*cos(theta_)*dt;
    		dx_dtheta_G= -v_*sin(theta_)*dt;


    		dx_dv_V=cos(theta_)*dt;
    		dx_dw_V=-0.5*(dt*dt)*v_*sin(theta_);
    		dy_dv_V=dt*sin(theta_);
    		dy_dw_V=0.5*(dt*dt)*v_*cos(theta_);
    		dtheta_dv_V=0;
    		dtheta_dw_V=dt;

    		dx=dy_dtheta_G;
    		//wolfram alpha: so eingeben
    		//limit ((v/w)*cos(theta) - (v/w)*cos(theta + w*d_t)   as w->0 )
    		dy=v_*dt*sin(theta_);

    	}

    	V=cv::Matx<double, 3,2>(dx_dv_V,dx_dw_V,
    							dy_dv_V,dy_dw_V,
								dtheta_dv_V,dtheta_dw_V);

    	cv::Matx<double, 2,3> V_transposed;
    	double err1=config_.alpha_1*v_*v_ + config_.alpha_2*w_*w_;
    	double err2=config_.alpha_3*v_*v_ + config_.alpha_4*w_*w_;
    	M=cv::Matx<double, 2,2> (err1,0,
    	    					0,err2);

    	G=cv::Matx<double, 3,3>(1,0,dx_dtheta_G,
    	   							0,1,dy_dtheta_G,
    									0,0,1);

    	cv::Matx<double, 3,3> G_transposed;

        xp[0] = x_ + dx;//Pedicted Pose
        xp[1] = y_ + dy;
        xp[2]= theta_+ w_*dt;

        cv::transpose(G,G_transposed);
        cv::transpose(V,V_transposed);

        Pp = G*P*G_transposed + V*M*V_transposed;//Predicted Covariance Calculation - line 7
       // printf("%f , %f , %f, %f , %f, %f\n",Pp.val[0],Pp.val[1],Pp.val[2],Pp.val[3],Pp.val[4],Pp.val[5]);
#endif
    } else {
        xp = x;
        Pp = P;
    }
    pose_predicted_ = xp;
}
void KalmanFilter::correction () {

    xc = pose_predicted_.state_vector();
    Pc = Pp;

    double dalpha, drho;
    Q = cv::Matx<double, 2,2> ( config_.sigma_alpha, 0, 0, config_.sigma_rho );
    char msg[0x1FF];
    msgs_.clear();
    for ( size_t idx_measurement = 0; idx_measurement < measurement_match_.size(); idx_measurement++ ) {
        int idx_map = measurement_match_[idx_measurement];
        if(idx_map < 0)continue;
        //printf("idx_map:%d\n",idx_map);
	cv::Matx<double, 2,3> H;   /// Check slides
	cv::Matx<double, 2, 1> v;  /// Messurment error between predition (known data) and detetion --> Siegwart;
	cv::Matx<double, 2,2> Si;  /// Check slides
	cv::Matx<double, 1,1> d_mahalanobis; // just for debugging reasons, not needed;
	cv::Matx<double, 3,2> K;   /// Kalman gain
	cv::Matx<double, 3,1> dx;  /// State change
        /**
        * @ToDo correction
        * Pose correction must update the KalmanFilter::xc and KalmanFilter::Pc which reprecents the corrected pose with covaraiance
        * have a look into Siegwart 2011 section 5.6.8.5 Case study: Kalman filter localization with line feature extraction
        **/
#if EXERCISE >= 42      
#else

	 /// first the prediciton and the measurment into polar space and compute the distance
	// v = ?
	Polar2D measurement_ = measurement_linesegments_[idx_measurement].toPolar();
	Polar2D  map_=map_linesegments_[idx_map].toPolar();
	double w_rho=map_.rho(),w_alpha=map_.alpha();
	cv::Matx<double, 2, 1>z_predicted_R;
	if( w_rho > (pose_predicted_.x()*cos(w_alpha) + pose_predicted_.y()*sin(w_alpha)) )
	{
		z_predicted_R.val[0]=angle_difference(w_alpha,pose_predicted_.theta() ) ;
		z_predicted_R.val[1]= w_rho-(pose_predicted_.x()*cos(w_alpha)+ pose_predicted_.y()*sin(w_alpha));
		// H = ?
		H=cv::Matx<double, 2,3>(0   ,    0   ,        -1,
						-cos(w_alpha),  -sin(w_alpha), 0);

	}else
	{
		z_predicted_R.val[0]=angle_difference(w_alpha+ M_PI,pose_predicted_.theta() ) ;
		z_predicted_R.val[1]= pose_predicted_.x()*cos(w_alpha)+ pose_predicted_.y()*sin(w_alpha) - w_rho;
		H=cv::Matx<double, 2,3>(0   ,   0   ,         -1,
						cos(w_alpha),  sin(w_alpha),  0);
	}

    v.val[0] = angle_difference(measurement_.alpha() , 	z_predicted_R.val[0]);						//INNOVATION
    v.val[1] = measurement_.rho() - z_predicted_R.val[1];


    // Si = ?
    Si=(H*Pp*H.t()+ Q).inv(); // = E_in_i,j^(-1)
    //Covariance_IN for Innovation

    // d_mahalanobis = ?
    d_mahalanobis=v.t()*Si*v;// just for debugging reasons, not needed;

   // K = ?
    K=Pp*H.t()*Si;

   // dx = ?
    dx=K*v;
   // Pc = ?
    cv::Matx<double, 3,3> I(1,0,0,
    						0,1,0,
							0,0,1);
    Pc=(I-K*H)*Pp;

   // xc = ?
    xc.val[0]=pose_predicted_.x()+dx.val[0];
    xc.val[1]=pose_predicted_.y()+dx.val[1];
    xc.val[2]=pose_predicted_.theta()+dx.val[2];

#endif
    }

    if ( config_.enable_correction ) {
        pose_estimated_ = xc;
        P = Pc;
    } else {
        P = Pp;
        pose_estimated_ =  pose_predicted_.state_vector();
    }
}
