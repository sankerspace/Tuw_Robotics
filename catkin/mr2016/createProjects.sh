#!/bin/bash
##################################################################
#With the new catkin_tools, there are few changed from the Catkin-y #method described above. To generate eclipse-project you need to #execute:

#catkin build  --force-cmake -G"Eclipse CDT4 - Unix Makefiles" 
#inside folder $MR_DIR
#to generate the .project files for each package and then run: the #follwoing script 
##################################################################

ROOT=$PWD 
cd build
for PROJECT in `find $PWD -name .project`; do
    DIR=`dirname $PROJECT`
    echo $DIR
    cd $DIR
    awk -f $(rospack find mk)/eclipse.awk .project > .project_with_env && mv .project_with_env .project
done
cd $ROOT
